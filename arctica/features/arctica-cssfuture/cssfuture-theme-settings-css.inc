<?php
global $abs_base_theme_path;
$selectors = theme_get_setting('process_selectors', $theme);
if ($selectors) {
  $CSS .= "\n\n";
  $CSS .= "$selectors {behavior:url({$abs_base_theme_path}/features/arctica-cssfuture/PIE/PIE_uncompressed.htc);}\n";
}