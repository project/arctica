<?php

/**
 * Prepare system messages for advanced theming
 * 'messages' class is included for backward compatibility with core css
 */
function arctica_status_messages($variables) {
  $display = $variables['display'];
  $output = '';
  foreach (drupal_get_messages($display) as $type => $messages) {
    foreach ($messages as $message) {
      $output .= "<div class=\"message grid-inner messages $type\">\n";
      $output .= '  <p>' . $message . "</p>\n";
      $output .= "</div>\n";
    }
  }
  return $output;
}

/**
 * Breadcrumb with title
 */
function arctica_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];

  if (!empty($breadcrumb)) {
    $output = '';
    $breadcrumb[] = truncate_utf8(drupal_get_title(), 60, $wordsafe = TRUE, $dots = TRUE);
    $output .= '<div class="breadcrumb">' . implode(' &raquo; ', $breadcrumb) . '</div>';
    return $output;
  }
}